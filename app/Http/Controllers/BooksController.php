<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index(){
        $books = \App\Book::all()
                ->where("id", "<",30)
                ->load("author")
                ->load("category");
        
        return view("Books.index")->with("books",$books);
    }
}
