<div>
    <?php foreach($books as $book){ ?>
    <table style="border: dashed #ccc 1px; width: 400px;">
        
        <tr>
            <td><b>Book Name:</b></td>
            <td><?= $book->book_name ?></td>
        </tr>
        <tr>
            <td><b>Author:</b></td>
            <td><?= $book->author->author_name ?></td>
        </tr>
        <tr>
            <td><b>Category:</b></td>
            <td><?= $book->category->category_name ?></td>
        </tr>
    </table>
    <?php } ?>
</div>