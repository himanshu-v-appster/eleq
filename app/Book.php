<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function author(){
        return $this->belongsTo('App\Author', "author_id");
    }
    public function category(){
        return $this->belongsTo('App\Category', "category_id");
    }
}
